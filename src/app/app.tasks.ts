import startTrackingBlockchain from '../tasks/startTrackingBlockchain';
import startUpdatingEngraveCache from '../tasks/startUpdatingEngraveCache';
import { logger } from '../submodules/shared-library';

const DELAY_S = 60;

function tasks() {

    (async () => {
        logger.info(`Starting in about a ${DELAY_S} seconds or so...`);
        setTimeout(startTrackingBlockchain, DELAY_S * 1000);
        setTimeout(startUpdatingEngraveCache, DELAY_S * 1000);
    })();
}

export default tasks;
