import { mongo } from './submodules/shared-library/config';

declare global {
    namespace NodeJS {
        interface Global {
            __rootdir__: string;
        }
    }
}

global.__rootdir__ = '/app/dist';

import app from './app/app';
import * as mongoose from 'mongoose';
const Sentry = require('@sentry/node');
import { RewriteFrames } from '@sentry/integrations';

import { listenOnPort, logger } from './submodules/shared-library';

Sentry.init({
    dsn: 'https://1162e80c2ac0457aa5f20e170dda6ff3@sentry.engrave.dev/4',
    release: `${process.env.npm_package_name}@${process.env.npm_package_version}`,
    integrations: [
        new RewriteFrames({ root: global.__rootdir__ })
    ]});

( async () => {
    try {
        await mongoose.connect(mongo.uri, mongo.options)
        listenOnPort(app, 3000);
    } catch (error) {
        logger.warn('Error encountered while starting the application: ', error.message);
        process.exit(1);
    }
})();

