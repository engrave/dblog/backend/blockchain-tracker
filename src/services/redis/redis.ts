import getLastProcessedBlock from './actions/getLastProcessedBlock';
import setLastProcessedBlock from './actions/setLastProcessedBlock';
import pushUpdatesToQueue from './actions/pushUpdatesToQueue';
import deleteArticle from './actions/deleteArticle';

export {
    getLastProcessedBlock,
    setLastProcessedBlock,
    pushUpdatesToQueue,
    deleteArticle
}