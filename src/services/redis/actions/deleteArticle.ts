
import { logger } from "../../../submodules/shared-library";
import { removeArticle } from "../../../submodules/shared-library/services/cache/cache";

async function deleteArticle(username: string, permlink: string) {

    try {      
        return await removeArticle(username, permlink);
    } catch (error) {
        logger.error(error);
    }
}

export default deleteArticle;