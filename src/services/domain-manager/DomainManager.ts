import { handleServiceError } from '../../submodules/shared-library/hof';
import axios, { AxiosRequestConfig } from 'axios';
import { microservices } from '../../submodules/shared-library';

export class DomainManager {
    public static register = async (domain: string) => {
        return handleServiceError(async () => {
            const options: AxiosRequestConfig = {
                url: `http://${microservices.domains_manager}/domains/register`,
                method: 'POST',
                data: {domain}
            };

            const {data} = await axios(options);
        })
    }
}
