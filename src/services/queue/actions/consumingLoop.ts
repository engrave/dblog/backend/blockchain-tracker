import { consumeUpdate, consumingLoop } from "../queue";
import { logger } from "../../../submodules/shared-library";

export default async () => {
    try {
        
        await consumeUpdate();

        setTimeout(consumingLoop, 100);
        
    } catch (error) {
        logger.error(error);
        setTimeout(consumingLoop, 100);
    }
}