import { getDataFromUpdateString } from "../queue";
import { removeArticle, setArticleContent, addArticle } from '../../../submodules/shared-library/services/cache/cache';
import { logger, hive } from "../../../submodules/shared-library";
import getBlogFromUsernamePermlink from "../../../submodules/shared-library/services/cache/actions/articles/getBlogFromUsernamePermlink";

const Redis = require('ioredis');
const redis = new Redis({ host: "redis" });

export default async () => {
    try {

        const update = await redis.rpop('updates');

        if(update) {
            const {author, permlink} = getDataFromUpdateString(update);

            logger.info("Consumed article:", author, permlink);

            const article = await hive.api.getContentAsync(author, permlink);

            logger.info(" * Fetched article from blockchain:", author, permlink);

            return await setArticleContent(author, permlink, article);
         }

    } catch (error) {
        logger.error(error);
    }
}
