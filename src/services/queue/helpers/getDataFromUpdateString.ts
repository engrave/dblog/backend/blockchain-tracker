
export default (update: string) => {
    try {
        
        const [author, permlink] = update.split(':');

        if( !author || !permlink) {
            return null;
        }
        
        return {
            author,
            permlink
        }
        
    } catch (error) {
        return null;
    }
}