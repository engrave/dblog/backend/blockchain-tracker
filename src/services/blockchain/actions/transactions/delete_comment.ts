import { ifArticleExist, removeArticle } from '../../../../submodules/shared-library/services/cache/cache';
import getBlogFromUsernamePermlink
    from '../../../../submodules/shared-library/services/cache/actions/articles/getBlogFromUsernamePermlink';

const processDeleteComment = async (operation: any): Promise<any> => {
    const { author, permlink } = operation[1];

    if(await ifArticleExist(author, permlink)) {
        const blogId = await getBlogFromUsernamePermlink(author, permlink);
        await removeArticle(blogId, permlink);
    }

    return null;
};

export { processDeleteComment };
