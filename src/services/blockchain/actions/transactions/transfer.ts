import { ifArticleExist } from '../../../../submodules/shared-library/services/cache/cache';
import { logger } from '../../../../submodules/shared-library/utils';
import { DomainsDataService } from '../../../../dataServices/DomainsDataService';
import { IDomain } from '../../../../submodules/shared-library/interfaces/IDomain';
import { DomainManager } from '../../../domain-manager/DomainManager';

const processTransfer = async (operation: any, tx: any): Promise<any> => {
    const {from, to, amount, memo} = operation[1];
    const quantity = Number(amount.replace(' HIVE', '').replace(' HBD', ''));

    if (to === 'engrave') {
        try {
            logger.info(`Found incoming transfer from ${from}: ${amount} (${memo})`)
            const msg = JSON.parse(memo)
            if (msg) {
                if (msg.register) {

                    const domain: IDomain = await DomainsDataService.getById(msg.register);

                    if (!domain) {
                        logger.warn(`Invalid registration request ${msg.register}.`);
                        return null;
                    }

                    if(domain.paid) {
                        logger.warn(`Could not register domain again`);
                        await DomainsDataService.addLog(domain._id, `Could not register domain again`);
                        return null;
                    }

                    if ((amount.indexOf('HIVE') && domain.purchasePrice.hive == quantity) ||
                        (amount.indexOf('HBD') && domain.purchasePrice.hbd == quantity)) {
                        setTimeout(async () => {
                            tryRegisteringDomain(domain, tx, amount);
                        }, 100);
                    } else {
                        const msg = `Incorrect amount. Received ${amount}, should be ${domain.purchasePrice.hive} HIVE or ${domain.purchasePrice.hbd} HBD`;
                        logger.warn(msg);
                        await DomainsDataService.addLog(domain._id, msg);
                    }
                }
            }
            return null;
        } catch (error) {
            logger.error(error);
            return null;
        }

    }

    return null;
};

const tryRegisteringDomain = async (domain: IDomain, tx: any, amount: string) => {
    try {
        logger.info(`Registering domain ${domain.domain} (${domain._id})`);
        await DomainManager.register(domain.domain);
        await DomainsDataService.setRegistered(domain._id, tx.transaction_id, tx.block_num, amount);
        await DomainsDataService.addLog(domain._id, 'Domain registered successfully');
        logger.info('Domain registered');
    } catch (error) {
        logger.error(error);
        await DomainsDataService.addLog(domain._id, 'Unexpected error occured while registering domain');
    }
}

export { processTransfer };
