import { ifArticleExist } from '../../../../submodules/shared-library/services/cache/cache';
import votedForWitness from '../../../../submodules/shared-library/services/cache/actions/witness/votedForWitness';
import setVotedForWitness
    from '../../../../submodules/shared-library/services/cache/actions/witness/setVotedForWitness';
import IUpdate from '../../helpers/IUpdate';
import { DiscordSender } from '../../../DiscordSender';
import { logger } from '../../../../submodules/shared-library/utils';

const processAccountWitnessVote = async (operation: any): Promise<IUpdate> => {
    const { account, witness, approve } = operation[1];

    logger.info(`Processing witness vote: ${account}, ${witness}, ${approve}`);

    if(witness === 'engrave' && approve === true && !await votedForWitness(account)) {
        await setVotedForWitness(account);
        await DiscordSender.send(`**${account}** approve witness **engrave**`);
        return null;
    }
    return null;
};

export { processAccountWitnessVote };
