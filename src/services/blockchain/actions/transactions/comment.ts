import { ifArticleExist } from '../../../../submodules/shared-library/services/cache/cache';

const processComment = async (operation: any) => {
    const { author, permlink } = operation[1];

    if(await ifArticleExist(author, permlink)) {
        return {author: author, permlink: permlink};
    }
};

export { processComment };
