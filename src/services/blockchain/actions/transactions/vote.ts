import { ifArticleExist } from '../../../../submodules/shared-library/services/cache/cache';

const processVote = async (operation: any) => {
    const { author, permlink } = operation[1];

    if(await ifArticleExist(author, permlink)) {
        return {author: author, permlink: permlink};
    }

    return null;
};

export { processVote };
