import { IProcessedBlock, processTransaction, IUpdate } from "../blockchain";
import { pushUpdatesToQueue } from "../../redis/redis";
import isUpdateInArray from '../helpers/isUpdateInArray';
import { logger, hive } from "../../../submodules/shared-library";

export default async (blockNumber: number): Promise<IProcessedBlock> => {
    try {
        const {transactions, timestamp} = await hive.api.getBlockAsync(blockNumber);

        if(transactions) {

            logger.info(`Transactions: ${transactions.length}, block: ${blockNumber}`);

            let updates: IUpdate[] = [];

            for(const tx of transactions) {
                const update = await processTransaction(tx);
                if(update && !isUpdateInArray(update, updates) ) {
                    logger.info('Found Engrave article:', update);
                    updates.push(update);
                }
            }

            pushUpdatesToQueue(updates);

            return {
                number: blockNumber,
                timestamp: new Date(timestamp + "Z").getTime()
            };
        }

    } catch (error) {
        return null;
    }
}
