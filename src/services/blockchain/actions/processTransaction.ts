
import { IUpdate, processTransaction } from "../blockchain";
import { logger } from "../../../submodules/shared-library";
import { ifArticleExist, removeArticle } from "../../../submodules/shared-library/services/cache/cache";
import getBlogFromUsernamePermlink from "../../../submodules/shared-library/services/cache/actions/articles/getBlogFromUsernamePermlink";
import { processVote } from './transactions/vote';
import { processComment } from './transactions/comment';
import { processDeleteComment } from './transactions/delete_comment';
import { processTransfer } from './transactions/transfer';
import { processAccountWitnessVote } from './transactions/account_witness_vote';

export default async (tx: any): Promise<IUpdate> => {
    try {

        for (const operation of tx.operations) {
            const type = operation[0];
            switch(type) {
                case 'vote':
                    return processVote(operation);
                    break;
                case 'comment':
                    return processComment(operation)
                    break;
                case 'delete_comment':
                    return processDeleteComment(operation);
                    break;
                case 'transfer':
                    return processTransfer(operation, tx);
                    break;
                case 'account_witness_vote':
                    return processAccountWitnessVote(operation);
                    break;
                default:
                    break;
            }
        }

        return null;
    } catch (error) {
        logger.error(error);
        return null;
    }
}
