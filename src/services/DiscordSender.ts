import axios from 'axios';
import { logger } from '../submodules/shared-library/utils';

export class DiscordSender {
    public static send = async (message: string) => {
        try {
            if(process.env.DISCORD_WITNESS_WEBHOOK) {
                await axios.post(process.env.DISCORD_WITNESS_WEBHOOK, { content: message});
            }
        } catch (error) {
            logger.error(error);
        }
    }
}
