import { IDomain } from '../submodules/shared-library/interfaces/IDomain';
import { Domains } from '../submodules/shared-library/models/Domains';
import * as moment from 'moment';

export class DomainsDataService {
    public static getById = async (id: string): Promise<IDomain> => {
        return DomainsDataService.getOneByQuery({_id: id});
    }

    public static addLog = async (id: string, msg: string): Promise<IDomain> => {
        return Domains.updateOne({_id: id}, {$push: {logs: {date: new Date(), msg}}});
    }

    public static setRegistered = async (id: string, transaction_id: string, block_num: number, amount: string): Promise<IDomain> => {
        await Domains.updateOne({_id: id}, {
            $set: {
                payment: {
                    transaction_id, block_num, amount
                },
                paid: true,
                creationDate: moment(),
                expirationDate: moment().add(364, 'day')
            }
        });
        return DomainsDataService.getOneByQuery({_id: id});
    }

    private static getOneByQuery = async (query: any): Promise<IDomain> => {
        return Domains.findOne(query);
    }

}
