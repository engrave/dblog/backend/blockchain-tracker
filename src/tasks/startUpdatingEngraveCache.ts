import { consumingLoop } from '../services/queue/queue';
import { logger } from '../submodules/shared-library';

export default async () => {
    logger.info('Started updating Engrave cache');

    await consumingLoop();
    
}