import { processBlockLoop } from '../services/blockchain/blockchain';
import { getLastProcessedBlock } from '../services/redis/redis';
import { logger } from '../submodules/shared-library';

export default async () => {

    let startingBlock = parseInt(process.env.STARTING_BLOCK);
    const lastProcessedBlock = await getLastProcessedBlock();

    if(lastProcessedBlock) {
        logger.info("Found last processed block number in redis: ", lastProcessedBlock);
        startingBlock = lastProcessedBlock;
    }

    await processBlockLoop(startingBlock);

}
