import getDataFromUpdateString from '../../../../../src/services/queue/helpers/getDataFromUpdateString';
import IUpdate from '../../../../../src/services/blockchain/helpers/IUpdate';
import { describe, it } from 'mocha';
import { expect } from 'chai';

const prepareUpdate = (author: string, permlink: string) => ({ author, permlink })
const prepareUpdateString = (update: IUpdate) => `${update.author}:${update.permlink}`;

const updates = [
    prepareUpdate('engrave', 'test'),
    prepareUpdate('engrave', 'example-permlink'),
]

const invalidArguments = [
    'invalid',
    null,
    undefined,
    {},
    []
]

describe('getDataFromUpdateString', () => {

    updates.forEach((update) => {
        it('Should return valid object from update string', () => {
            const updateString: string = prepareUpdateString(update);
            const result = getDataFromUpdateString(updateString);
            expect(result).to.be.deep.equal(update);
        })
    })

    invalidArguments.forEach( invalidArgument => {
        it('Should return null when invalid argument is passed', () => {
            const result = getDataFromUpdateString(invalidArgument as any);
            expect(result).to.be.null;
        }) 
    })

})