import isUpdateInArray from '../../../../../src/services/blockchain/helpers/isUpdateInArray';
import IUpdate from '../../../../../src/services/blockchain/helpers/IUpdate';
import { describe, it } from 'mocha';
import { expect } from 'chai';

const prepareUpdate = (author: string, permlink: string): IUpdate => ({ author, permlink })

const updates: IUpdate[] = [
    prepareUpdate('engrave', 'testing-one'),
    prepareUpdate('engrave', 'testing-two'),
    prepareUpdate('bgornicki', 'testing-one'),
]

describe('isUpdateInArray', () => {

    updates.forEach((update) => {
        it('Should return true for valid updates in array', () => {
            const result = isUpdateInArray(update, updates);
            expect(!!result).to.be.true;
        })
    })

    it('Should return false for update which is not in array', () => {
        const result = isUpdateInArray(prepareUpdate('non', 'exist'), updates);
        expect(!!result).to.be.false; 
    })
    
})